package Testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class AcmeSearchVendor {
	@Test
	public void search() {
		System.setProperty("webdriver.chrome.driver", "./drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("manjili.sudharson@gmail.com");
		driver.findElementById("password").sendKeys("manjili2109");
		driver.findElementById("buttonLogin").click();
		WebElement vendor = driver.findElementByXPath("//button[text()=' Vendors']");
		System.out.println(vendor.getText());
		Actions builder = new Actions(driver);
		builder.moveToElement(vendor).perform();
		driver.findElementByLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys("FR121212");
		driver.findElementById("buttonSearch").click();
		WebElement text = driver.findElementByXPath("//table[@class='table']/tbody/tr/following::tr/td");
		System.out.println(text.getText());
		

	}

}
